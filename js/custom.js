
function setSectionHeight(){

	var docHeight = $('.background').height();

	var sectionHeights = {

		"ufo-section" : 0.25,
		"comet-section" : 0.36,
		"landing-section" : 0.298,
		"spacer-one" : 0.03,
		"spacer-two" : 0.06
	}

	$.each(sectionHeights,function(index,value){

		
		$( "." + index ).height( Math.floor( value * docHeight ) );

	});

}

	
$(function(){

	setSectionHeight();

});


$(window).resize(function(){

	setSectionHeight();

});